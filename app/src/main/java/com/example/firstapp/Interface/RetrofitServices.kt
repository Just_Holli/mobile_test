package com.example.firstapp.Interface


import com.example.firstapp.Models.Photo
import com.example.firstapp.Models.photos
import retrofit2.http.GET


interface RetrofitServices {
    @GET("posts/")
    suspend fun getPosts(): List<photos>

    @GET("photos/")
    suspend fun getPhotos(): List<Photo>
}
