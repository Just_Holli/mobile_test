package com.example.firstapp.Models

enum class Status {
    Success,
    Error,
    Loading
}
