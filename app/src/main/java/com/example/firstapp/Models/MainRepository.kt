package com.example.firstapp.Models

import com.example.firstapp.Retrofit.ApiHelper

class MainRepository(private val apiHelper: ApiHelper) {
    suspend fun getPosts() = apiHelper.getPosts()
    suspend fun getPhotos() = apiHelper.getPhotos()
}