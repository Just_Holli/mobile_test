package com.example.firstapp.Models

import android.icu.text.CaseMap
import com.google.gson.annotations.SerializedName

data class photos(
    @SerializedName("userId")
    var userId: Int?,
    @SerializedName("id")
    var postId: Int?,
    @SerializedName("title")
    var title: String?,
    @SerializedName("body")
    var text: String?
)

