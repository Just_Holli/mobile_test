package com.example.firstapp.Retrofit

import com.example.firstapp.Interface.RetrofitServices

class ApiHelper(private val retrofitServices: RetrofitServices) {
    suspend fun getPosts() = retrofitServices.getPosts()
    suspend fun getPhotos() = retrofitServices.getPhotos()
}