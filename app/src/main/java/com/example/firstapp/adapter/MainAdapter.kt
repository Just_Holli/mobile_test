package com.example.firstapp.adapter

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.app.NotificationCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.firstapp.MainActivity
import com.example.firstapp.Models.Photo
import com.example.firstapp.Models.photos
import com.example.firstapp.R
import kotlinx.android.synthetic.main.list_item.view.*
import java.io.File

class MainAdapter(
    private val photos: ArrayList<Photo>,
    private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    inner class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        fun bind(photo: Photo) {
            itemView.apply {
                textViewTittle.text = photo.title
                Glide
                    .with(context)
                    .load(photo.thumbnailUrl + ".jpg")
                    .error(R.mipmap.ic_launcher)
                    .into(preview)
            }
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position: Int = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position,photos[position])
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int,photo: Photo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        )

    override fun getItemCount(): Int = photos.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(photos[position])
    }

    fun addPosts(photos: List<Photo>) {
        this.photos.apply {
            clear()
            addAll(photos)
        }
    }
}