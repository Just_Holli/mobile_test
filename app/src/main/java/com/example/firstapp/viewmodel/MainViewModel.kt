package com.example.firstapp.viewmodel

import androidx.lifecycle.*

import com.example.firstapp.Models.MainRepository
import kotlinx.coroutines.Dispatchers
import com.example.firstapp.Models.Result


class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {


    fun getPhotos() = liveData(Dispatchers.IO) {
        emit(Result.loading(data = null))
        try {
            emit(Result.success(data = mainRepository.getPhotos()))
        } catch (exception: Exception) {
            emit(Result.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
//fun getPost1() = liveData(Dispatchers.IO) {
//        emit(Result.loading(data = null))
//        try {
//            emit(Result.success(data = mainRepository.getPosts()))
//        } catch (exception: Exception) {
//            emit(Result.error(data = null, message = exception.message ?: "Error Occurred!"))
//        }
//    }
//
//    private val _post= MutableLiveData<Post>()
//    val post: LiveData<Post>
//        get() = _post
//    fun getPost2(){
//        viewModelScope.launch {
//            val result = mainRepository.getPosts()
//            _post.postValue(result.data)
//        }
//    }
}