package com.example.firstapp

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firstapp.Models.Photo
import com.example.firstapp.Models.Status
import com.example.firstapp.Retrofit.ApiHelper
import com.example.firstapp.Retrofit.RetrofitBuilder
import com.example.firstapp.adapter.MainAdapter
import com.example.firstapp.viewmodel.MainViewModel
import com.example.firstapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainAdapter.OnItemClickListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    private var flag = true

    companion object {
        const val channelId = "channelID"
        const val channelName = "channelName"
        const val descriptionText = "i.apps.notifications"

        @RequiresApi(Build.VERSION_CODES.N)
        const val importance = NotificationManager.IMPORTANCE_DEFAULT

        @SuppressLint("StaticFieldLeak")
        lateinit var builder: NotificationCompat.Builder
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupUI()
        setupObservers()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun setupUI() {
        recyclerMovieList.layoutManager = LinearLayoutManager(this)
        adapter = MainAdapter(arrayListOf(), this)
        recyclerMovieList.addItemDecoration(
            DividerItemDecoration(
                recyclerMovieList.context,
                (recyclerMovieList.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerMovieList.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.getPhotos().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.Success -> {
                        recyclerMovieList.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        resource.data?.let { photos -> retrieveList(photos) }
                    }
                    Status.Error -> {
                        recyclerMovieList.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.Loading -> {
                        progressBar.visibility = View.VISIBLE
                        recyclerMovieList.visibility = View.GONE
                    }
                }
            }
        })
    }

    private fun retrieveList(photos: List<Photo>) {
        adapter.apply {
            addPosts(photos)
            notifyDataSetChanged()
        }
    }


    override fun onItemClick(position: Int, photo: Photo) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel(channelId, channelName, importance).apply {
                    description = descriptionText
                }
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
        builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Новое уведомление")
            .setContentText(photo.title)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        flag = if (flag) {
            with(NotificationManagerCompat.from(this)) {
                notify(position, builder.build()) // посылаем уведомление
            }
            false
        } else {
            NotificationManagerCompat.from(this).cancel(position)
            true
        }
        adapter.notifyItemChanged(position)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }
}