Необходимо разработать приложение для Android. Приложение должно:

1. ходить на публичное api
2. отображать результат запроса к этому api в виде списка (ListView)
3. каждая строка списка содержит заголовок и картинку-preview
4. тап (клик) на строку создает уведомление в области уведомлений, повторный тап скрывает уведомление

![alt tag](http://surl.li/aasri "Screenshot приложения")​
![alt tag](http://surl.li/aasrm "Screenshot уведомления")
